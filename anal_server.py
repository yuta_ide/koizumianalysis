#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import sys
import io
import json
from bottle import route, run, request, response, static_file
import requests
from anal import extractKeyword, createWordMatrix, analMatrix
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot
from boto.s3.connection import S3Connection
from boto.s3.connection import Location as S3Location
from boto.s3.key import Key

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
AWS_KEY_ID = '<AWS_KEY_ID>'
AWS_SECRET_KEY = '<AWS_SECRET_KEY>'
BUCKET_NAME = '18thtestbucket'
KEY_NAME = '18th_key'

# routeデコレーター
# これを使用してURLのPathと関数をマッピングする。
@route('/hello')
def hello():
  return "Hellfssso Worlds!f"

@route('/anal')
def anal():
    data = requests.get('http://127.0.0.1:3000/api/v1/comments').json()
    comments = []
    for i in range(len(data)):
        tmp = []
        d = data[i]
        tmp.append(extractKeyword(str(d["content"])))
        comments.append(tmp)
    wm = createWordMatrix(comments)
    analMatrix(wm)
    image = open('./output.png', 'rb')
    files = {'image[image]': ('output.png', image, 'image/png')}
    res = requests.post('http://127.0.0.1:3000/api/v1/images', files=files)
    return str(analMatrix(wm))

@route('/<file_path:path>')
def static(file_path):
    return static_file(file_path, root='./')

# ビルトインの開発用サーバーの起動
# ここでは、debugとreloaderを有効にしているc
run(host='127.0.0.1', port=8080, debug=True, reloader=True)
