FieldSonar-analysis
====

v1.2がリリースされました. annotateが見やすくなりました

## Demo

## 機能

#### 文からの名詞取得
Mecabの利用

#### キーワード行列の生成
コメントid, 単語の表に, 出現数が記入される

#### 次元圧縮
PCAをscikit-learnから叩く

#### 可視化
matplotlibで結果を散布図として描画

## benri command

Pythonサーバー立ち上げ
```
python3 anal_server.py
```

機械学習実行
```
// kousin.sh

curl http://127.0.0.1:8080/anal
```

定期実行
```
// 10秒ごとに実行

while true; do sh kousin.sh; sleep 10; done &
```

## 起動方法

```
python3 anal_server.py
while true; do sh kousin.sh; sleep 10; done &
```

ローカルサーバーがアクセスを検知すると, ローカルサーバーがherokuにcommentのデータを取りに行きます. そしてそのコメントを機械学習して画像を生成します. そしてその画像をherokuにPOSTします. 

## v1.1からの変更点

+ annotateをランダムにすることで見やすくなりました

## 課題
* キーワード行列の生成にバグがあるかもしれない. 確かめること.