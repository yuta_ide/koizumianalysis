#coding:utf-8
from natto import MeCab
import pandas as pd
from sklearn.decomposition import PCA
import numpy as np
from sklearn import manifold
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

mc = MeCab()
fp = FontProperties(fname='./gothic.ttc')

def extractKeyword(text):
    words = []
    with MeCab('-F%m,%f[0],%h') as nm:
        for n in nm.parse(text, as_nodes=True):
            node = n.feature.split(',');
            if len(node) != 3:
                continue
            if node[1] == '名詞':
                words.append(node[0])
    return words

def createWordMatrix(array):
    #array: [[hoge, foo], [hoge, fugafa], ...]
    print("createWordMatrix() fire")
    print("arrayy: ")
    print(array)
    cmtCol = list(range(len(array)))
    wordIndex = []
    for i in array:
        for j in i:
            wordIndex.extend(j)
    print(wordIndex)
    wordIndex = list(set(wordIndex))
    df = pd.DataFrame(0, index = cmtCol, columns = wordIndex)
    i_cnt = 0
    j_cnt = 0
    for i in range(len(cmtCol)):
        mat_row = df.iloc[i]
        print("mat_row")
        print(mat_row)
        j_cnt = 0
        for j in mat_row:
            for k in array[i]:
                for l in array[i][0]:
                    if wordIndex[j_cnt] == l:
                        mat_row[j_cnt] = mat_row[j_cnt] + 1
                    else:
                        print(wordIndex[j_cnt])
            j_cnt = j_cnt + 1
    return df

def analMatrix(df):
    print("df.columnffs")
    print(list(df.columns))
    label = list(df.columns)
    df = df.T
    pca = PCA(n_components=2)
    X_r = pca.fit(df).transform(df)
    fig, ax = plt.subplots()
    for i, txt in enumerate(label):
        ax.scatter(X_r[i,0],X_r[i,1], label=label[i])
        ax.annotate(txt, (X_r[i,0],X_r[i,1]), fontproperties=fp)

    filename = "./output.png"
    plt.savefig(filename)
    plt.close()
    return X_r
